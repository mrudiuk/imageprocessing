BASE_DIR = './image'
CANNY_DIR = BASE_DIR + '/canny/'
SOBEL_DIR = BASE_DIR + '/sobel/'
SOURCE_DIR = BASE_DIR + '/source/'
FILE_NAME = 'Bikesgray'
FILE_TYPE = 'jpg'
ABSOLUTE_PATH = SOURCE_DIR+FILE_NAME+'.'+FILE_TYPE


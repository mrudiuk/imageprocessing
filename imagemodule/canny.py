import numpy as np
from PIL import Image
import math
from src.imagemodule import resize
from src.mathmodule import generate, gauss


def normalize_image(img):
    """

    :param img: blurred image
    :return: normalize image
    """
    norm_img = Image.new('F', img.size)
    for i in range(img.width):
        for j in range(img.height):
            pix = img.getpixel((i, j)) / 255
            norm_img.putpixel((i, j), round(pix))

    return norm_img


def get_kernel(kernel_number):

    if kernel_number == 1:
        Gx_kernel = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])
        return Gx_kernel
    elif kernel_number == 2:
        Gy_kernel = np.array([[-1, -2, -1], [0, 0, 0], [1, 2, 1]])
        return Gy_kernel
    elif kernel_number == 3:
        G_kernel = np.array([[1, -2, 1], [2, -4, 2], [1, -2, 1]])
        return G_kernel
    else:
        raise ValueError


def convolution(img, kernel_number):

    kernel = get_kernel(kernel_number)

    resize_img = resize.custom_resize(img, 2, 1)
    template_matrix = generate.generate_matrix([3, 3])
    result_img = Image.new(img.mode, img.size)
    for i in range(img.width):
        for j in range(img.height):
            pix = 0

            for element in template_matrix:
                    a = element[0]
                    b = element[1]
                    pix += kernel[1+a][1+b] * resize_img.getpixel((i + 1 + a, j + 1 + b))

            result_img.putpixel((i, j), int(pix))

    return result_img


def sobel(img):

    Gx = convolution(img, 1)
    Gy = convolution(img, 2)
    sobel_img = Image.new(Gx.mode, Gx.size)

    for i in range(Gx.width):
        for j in range(Gx.height):
            pix = math.sqrt((math.pow(Gx.getpixel((i, j)), 2))+(math.pow(Gy.getpixel((i, j)), 2)))
            sobel_img.putpixel((i, j), round(pix))

    return sobel_img


def canny_edge(img):
    source = sobel(img)
    canny = Image.new(img.mode, img.size)

    for i in range(source.width):
        for j in range(source.height):
            if 200 < source.getpixel((i, j)) < 255:
                canny.putpixel((i, j), 255)
            if 0 < source.getpixel((i, j)) < 50:
                canny.putpixel((i, j), 255)

    return canny



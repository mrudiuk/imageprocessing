from PIL import Image


def custom_resize(img, resize_size, range_value):

    resize_img = img.resize((img.width + resize_size, img.height + resize_size), Image.BICUBIC)
    for i in range(img.width):
        for j in range(img.height):
            resize_img.putpixel((i+range_value, j+range_value), img.getpixel((i, j)))

    return resize_img

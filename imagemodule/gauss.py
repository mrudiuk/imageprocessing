from PIL import Image
from src.mathmodule import generate, gauss
from src.imagemodule import gray, resize


# import numpy as np


def gauss_blur(img, matrix_size, sigma):
    """

    :param matrix_size: It is array in two elements [n, n]. It is matrix size. :type integer. :example [5, 5]
      IMPORTANT! n = n, if not, we have ValueError.
    :param sigma: This param control blur scale
    :param img: source image - calculate settings.py
    :return: blurred image
    """

    resize_value = matrix_size[0]//2*2  # значення на яке збільшується вхідне зображення для обробки границь
    range_value = matrix_size[0] // 2  # значення яке коригує початок ітерації по збільшеному зображені
    source_image = gray.to_gray_scale(img)  # переводимо вихідне зображення в градації сірого

    resize_img = resize.custom_resize(source_image, resize_value, range_value)  # збільшення зображення
    new_image = Image.new(source_image.mode, source_image.size)  # стрюємо нове зображення
    weight_matrix = gauss.gauss_coefficient(sigma, matrix_size)  # розраховуємо вагову матрицю
    template_matrix = generate.generate_matrix(matrix_size)  # розраховуємо матрицю шаблон

    for i in range(source_image.width):
        for j in range(source_image.height):
            data = []
            for element in template_matrix:
                data.append(resize_img.getpixel((i + range_value + element[0], j + range_value + element[1])))

            center_pix = 0
            for y in range(len(data)):
                pix = data[y] * weight_matrix[y]
                center_pix += pix

            new_image.putpixel((i, j), round(center_pix))

    return new_image

from PIL import Image


def to_gray_scale(img):
    """

    :param img - source image
    :return: grayscale image
    """
    if img.mode == 'L' or img.mode == '1':
        return img
    else:
        source_image = img
        gray_scale_image = Image.new('L', source_image.size)

        for i in range(source_image.width):
            for j in range(source_image.height):
                r, g, b = source_image.getpixel((i, j))
                gray_color = round(0.2126*r + 0.7152*g + 0.0722*b)
                gray_scale_image.putpixel((i, j), gray_color)

        return gray_scale_image

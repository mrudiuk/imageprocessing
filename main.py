from src.imagemodule import gauss, canny  # , resize
from src.settings import ABSOLUTE_PATH  # , SOBEL_DIR
from time import time
from PIL import Image
# from src.mathmodule import gauss, generate

begin_time = time()

img = Image.open(ABSOLUTE_PATH)
res = gauss.gauss_blur(img, [5, 5], 1.5)
sobel = canny.sobel(res)
sobel.show()
end_time = time()
run_time = end_time-begin_time

print('--------------------------------------------------')
print("Загальний час виконання - %s секунд" % round(run_time, 5))

import math
from src.mathmodule import generate


def gauss_coefficient(sigma, matrix_size):
    """

    :param matrix_size: It is array in two elements [n, n]. It is matrix size. :type integer. :example [5, 5]
      IMPORTANT! n = n, if not, we have ValueError.
    :param sigma: This param control blur scale
    # :param x: This param control matrix size
    :return: Weight coefficient gaussian matrix
    """
    input_matrix = generate.generate_matrix(matrix_size)
    output_matrix = []
    weight_matrix = []
    coeff_sum = 0

    for i in input_matrix:
        exp_pow = (0 - (math.pow(i[0], 2)+math.pow(i[1], 2)))/(2*math.pow(sigma, 2))
        coeff = (1/(2*math.pi*math.pow(sigma, 2)))*math.exp(exp_pow)
        coeff = round(coeff, 7)
        coeff_sum += coeff
        output_matrix.append(coeff)

    for i in output_matrix:
        coeff = round(i/coeff_sum, 7)
        weight_matrix.append(coeff)

    return weight_matrix

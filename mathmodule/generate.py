import numpy as np


def generate_matrix(matrix_size):
    """

    :param matrix_size: It is array in two elements, [n, n] - It is matrix size. :type integer. :example [5, 5]
      IMPORTANT! n = n, if not, we have ValueError.
    :return: coordinate matrix
    """
    h, w = matrix_size

    if h != w:
        raise ValueError

    interval = h // 2 or w // 2
    matrix = []
    x, y = np.meshgrid(np.linspace(0-interval, interval, h), np.linspace(interval, 0-interval, w))
    for i in range(len(x)):
        row_x = x[i]
        row_y = y[i]
        for j in range(len(row_x)):
            matrix.append([int(row_x[j]), int(row_y[j])])

    return matrix
